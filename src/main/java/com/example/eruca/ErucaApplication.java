package com.example.eruca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErucaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ErucaApplication.class, args);
    }

}
