package com.example.eruca.service;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.ManualControl;
import com.example.eruca.repository.BoxRepository;
import com.example.eruca.repository.ManualControlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManualControlService {

    @Autowired
    private ManualControlRepository manualControlRepository;

    @Autowired
    private BoxRepository boxRepository;

    public void createManualControl(long id, ManualControl manualControl) {
        Box box = boxRepository.getBoxById(id);
        manualControl.setBox(box);

        manualControlRepository.save(manualControl);
    }

    public void addValuesToManualControl(long id, ManualControl manualControl) {
        ManualControl control = manualControlRepository.getManualControlByBox(boxRepository.getBoxById(id));

        control.setBrightnessMin(manualControl.getBrightnessMin());
        control.setBrightnessMax(manualControl.getBrightnessMax());
        control.setFluidLevelMin(manualControl.getFluidLevelMin());
        control.setFluidLevelMax(manualControl.getFluidLevelMax());
        control.setTemperatureMin(manualControl.getTemperatureMin());
        control.setTemperatureMax(manualControl.getTemperatureMax());
        control.setHumidityMin(manualControl.getHumidityMin());
        control.setHumidityMax(manualControl.getHumidityMax());
        control.setSoilMoistureMin(manualControl.getSoilMoistureMin());
        control.setSoilMoistureMax(manualControl.getSoilMoistureMax());

        manualControlRepository.save(control);
    }

    public void removeManualControl(long id) {
        ManualControl control = manualControlRepository.getManualControlByBox(boxRepository.getBoxById(id));

        control.setBrightnessMin(0);
        control.setBrightnessMax(0);
        control.setFluidLevelMin(0);
        control.setFluidLevelMax(0);
        control.setTemperatureMin(0);
        control.setTemperatureMax(0);
        control.setHumidityMin(0);
        control.setHumidityMax(0);
        control.setSoilMoistureMin(0);
        control.setSoilMoistureMax(0);

        manualControlRepository.save(control);
    }

    public ManualControl getBoxManualControl(long id) {
        return manualControlRepository.getManualControlByBox(boxRepository.getBoxById(id));
    }
}
