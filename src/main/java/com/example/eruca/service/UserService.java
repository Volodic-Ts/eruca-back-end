package com.example.eruca.service;

import com.example.eruca.entity.User;
import com.example.eruca.entity.UserDTO;
import com.example.eruca.repository.UserRepository;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;

import java.util.List;

import static java.rmi.server.LogStream.log;

@Service
@Log
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JavaMailSender emailSender;

    public void register(UserDTO userDTO) {
        if (!checkIfUserExist(userDTO.getEmail(), userDTO.getLogin())) {
            if (userDTO.validateEmail(userDTO.getEmail()) &&
                    userDTO.validatePassword(userDTO.getPassword()) &&
                    userDTO.comparePasswords(userDTO.getPassword(), userDTO.getMatchPassword())) {
                User user = new User();
                int code = generateConfirmationCode();

                user.setLogin(userDTO.getLogin());
                user.setEmail(userDTO.getEmail());
                encodePassword(user, userDTO.getPassword());
                user.setRoles("ROLE_USER");
                user.setCode(code);

                sendConfirmationCode(userDTO.getEmail(), code);

                userRepository.save(user);
            } else {
                log.severe("Wrong data 1!");
            }
        } else {
            log.severe("Wrong data 2!");
        }
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUser(String login) {
        return userRepository.getUserByLogin(login);
    }

    public void deleteUser(String login) {
        User user = userRepository.getUserByLogin(login);

        userRepository.delete(user);
    }

    public void deleteUser(long id) {
        User user = userRepository.getUserByUserId(id);

        userRepository.delete(user);
    }

    public void confirmAccountByCode(int code) {
        User user = userRepository.getUserByCode(code);

        user.setActive(true);
        user.setCode(0);

        userRepository.save(user);
    }

    private void sendConfirmationCode(String mail, int code) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(mail);
        message.setSubject("Email confirmation");
        message.setText("Hi!\nYou need to enter this code in app to confirm your account!\n\n" + code +
                "\n\nGood luck!");

        this.emailSender.send(message);
    }

    private int generateConfirmationCode() {
        int leftLimit = 10000000;
        int rightLimit = 99999999;

        return leftLimit + (int) (Math.random() * (rightLimit - leftLimit));
    }

    private boolean checkIfUserExist(String email, String login) {
        return userRepository.getUserByEmail(email) != null || userRepository.getUserByLogin(login) != null;
    }

    private void encodePassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
    }
}
