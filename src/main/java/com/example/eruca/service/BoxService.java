package com.example.eruca.service;

import com.example.eruca.configuration.jwt.JwtUtil;
import com.example.eruca.controller.AuthController;
import com.example.eruca.entity.Box;
import com.example.eruca.entity.User;
import com.example.eruca.repository.BoxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoxService {

    @Autowired
    private BoxRepository boxRepository;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthController authController;

    public List<Box> findAllBoxes() {
        return boxRepository.findAll();
    }

    public Box findBoxById(long id) {
        return boxRepository.getBoxById(id);
    }

    public void addNewBox(Box box) {
        box.setId(box.generateBoxId());
        box.setUser(jwtUtil.getUserFromToken(authController.getAuthToken()));

        boxRepository.save(box);
    }

    public List<Box> getAllUserBoxes() {
        User user = jwtUtil.getUserFromToken(authController.getAuthToken());

        return boxRepository.getBoxesByUser(user);
    }

    public void editBox(long id, Box box) {
        Box myBox = findBoxById(id);

        myBox.setId(myBox.getId());
        myBox.setVersion(myBox.getVersion());

        myBox.setTitle(box.getTitle());

        boxRepository.save(myBox);
    }

    public void deleteBox(long id) {
        Box box = findBoxById(id);

        boxRepository.delete(box);
    }
}
