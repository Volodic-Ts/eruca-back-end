package com.example.eruca.service;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.ModulesState;
import com.example.eruca.repository.BoxRepository;
import com.example.eruca.repository.ModulesStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModulesStateService {

    @Autowired
    private ModulesStateRepository modulesStateRepository;

    @Autowired
    private BoxRepository boxRepository;

    public List<ModulesState> getAllModulesState() {
        return modulesStateRepository.findAll();
    }

    public ModulesState getBoxModulesState(long id) {
        Box box = boxRepository.getBoxById(id);

        return modulesStateRepository.getModulesStateByBox(box);
    }

    public void addModulesState(long id, ModulesState modulesState) {
        Box box = boxRepository.getBoxById(id);
        modulesState.setBox(box);

        modulesStateRepository.save(modulesState);
    }

    public void editModulesState(long id, ModulesState modulesState) {
        Box box = boxRepository.getBoxById(id);
        ModulesState modules = modulesStateRepository.getModulesStateByBox(box);

        modules.setBox(box);

        modules.setCooler(modulesState.isCooler());
        modules.setLight(modulesState.isLight());
        modules.setPump(modulesState.isPump());

        modulesStateRepository.save(modules);
    }
}
