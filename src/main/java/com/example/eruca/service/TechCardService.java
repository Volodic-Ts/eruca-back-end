package com.example.eruca.service;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.TechCard;
import com.example.eruca.repository.BoxRepository;
import com.example.eruca.repository.TechCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TechCardService {

    @Autowired
    private TechCardRepository techCardRepository;

    @Autowired
    private BoxRepository boxRepository;

    public List<TechCard> getAllTechCards() {
        return techCardRepository.findAll();
    }

    public TechCard getBoxTechCard(long id) {
        Box box = boxRepository.getBoxById(id);

        return techCardRepository.getTechCardByBox(box);
    }

    public void addTechCard(long id, TechCard techCard) {
        Box box = boxRepository.getBoxById(id);
        techCard.setBox(box);

        techCardRepository.save(techCard);
    }

    public void editTechCard(long id, TechCard techCard) {
        Box box = boxRepository.getBoxById(id);
        TechCard card = techCardRepository.getTechCardByBox(box);

        card.setBox(box);

        card.setBrightnessMin(techCard.getBrightnessMin());
        card.setBrightnessMax(techCard.getBrightnessMax());
        card.setFluidLevelMin(techCard.getFluidLevelMin());
        card.setFluidLevelMax(techCard.getFluidLevelMax());
        card.setHumidityMin(techCard.getHumidityMin());
        card.setHumidityMax(techCard.getHumidityMax());
        card.setTemperatureMin(techCard.getTemperatureMin());
        card.setTemperatureMax(techCard.getTemperatureMax());
        card.setSoilMoistureMin(techCard.getSoilMoistureMin());
        card.setSoilMoistureMax(techCard.getSoilMoistureMax());

        techCardRepository.save(card);
    }

    public void deleteTechCard(long id) {
        Box box = boxRepository.getBoxById(id);
        TechCard card = techCardRepository.getTechCardByBox(box);

        box.setTechCard(null);

        boxRepository.save(box);
        techCardRepository.delete(card);
    }

//    private List<TechCard> getAllBoxTechCards(long id) {
//        Box box = boxRepository.getBoxById(id);
//        List<TechCard> cards = getAllTechCards();
//        List<TechCard> filteredCards = new ArrayList<>();
//
//        for (TechCard card : cards) {
//            if (card.getBox().equals(box)) {
//                filteredCards.add(card);
//            }
//        }
//
//        return filteredCards;
//    }
}
