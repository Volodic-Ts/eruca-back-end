package com.example.eruca.service;

import com.example.eruca.entity.TechCardTemplate;
import com.example.eruca.repository.TechCardTemplatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TechCardTemplatesService {

    @Autowired
    TechCardTemplatesRepository repository;

    public List<TechCardTemplate> getAllTemplates() {
        return repository.findAll();
    }

    public TechCardTemplate getOneTemplate(int id) {
        return repository.getTechCardTemplatesByPlantId(id);
    }

    public void addTechCardTemplates(List<TechCardTemplate> templates) {
        repository.saveAll(templates);
    }

    public void deleteSavedTemplate(int id) {
        TechCardTemplate template = repository.getTechCardTemplatesByPlantId(id);

        repository.delete(template);
    }

    public void editSavedTemplatesIds(int n) {
        List<TechCardTemplate> templates = repository.findAll();

        for (TechCardTemplate template : templates) {
            template.setPlantId(template.getPlantId() - n);
        }
        repository.deleteAll();
        repository.saveAll(templates);
    }

    public void deleteAllTemplates() {
        repository.deleteAll();
    }
}
