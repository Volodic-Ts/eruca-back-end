package com.example.eruca.service;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.SensorsData;
import com.example.eruca.repository.BoxRepository;
import com.example.eruca.repository.SensorsDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SensorsDataService {

    @Autowired
    private SensorsDataRepository sensorsDataRepository;

    @Autowired
    private BoxRepository boxRepository;

    public List<SensorsData> getAllSensorsData() {
        return sensorsDataRepository.findAll();
    }

    public SensorsData getBoxSensorsData(long id) {
        return sensorsDataRepository.getSensorsDataByBox_Id(id);
    }

    public void addSensorsData(long id, SensorsData sensorsData) {
        Box box = boxRepository.getBoxById(id);
        sensorsData.setBox(box);

        sensorsDataRepository.save(sensorsData);
    }

    public void editSensorsData(long id, SensorsData sensorsData) {
        Box box = boxRepository.getBoxById(id);
        SensorsData data = sensorsDataRepository. getSensorsDataByBox(box);

        data.setBox(box);

        data.setBrightness(sensorsData.getBrightness());
        data.setFluidLevel(sensorsData.getFluidLevel());
        data.setHumidity(sensorsData.getHumidity());
        data.setSoilMoisture(sensorsData.getSoilMoisture());
        data.setTemperature(sensorsData.getTemperature());

        sensorsDataRepository.save(data);
    }
}
