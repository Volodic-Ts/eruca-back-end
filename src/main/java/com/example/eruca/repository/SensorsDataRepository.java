package com.example.eruca.repository;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.SensorsData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorsDataRepository extends JpaRepository<SensorsData, Integer> {
    SensorsData getSensorsDataByBox_Id(long id);
    SensorsData getSensorsDataByBox(Box box);
}
