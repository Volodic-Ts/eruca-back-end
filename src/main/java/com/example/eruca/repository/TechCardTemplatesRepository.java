package com.example.eruca.repository;

import com.example.eruca.entity.TechCardTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechCardTemplatesRepository extends JpaRepository<TechCardTemplate, Integer> {
    TechCardTemplate getTechCardTemplatesByPlantId(int id);
}
