package com.example.eruca.repository;


import com.example.eruca.entity.Box;
import com.example.eruca.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoxRepository extends JpaRepository<Box, Long> {
    Box getBoxById(long id);
    List<Box> getBoxesByUser(User user);
}
