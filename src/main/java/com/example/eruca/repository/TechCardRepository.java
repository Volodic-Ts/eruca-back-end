package com.example.eruca.repository;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.TechCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechCardRepository extends JpaRepository<TechCard, Integer> {
    TechCard getTechCardByBox(Box box);
}
