package com.example.eruca.repository;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.ManualControl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManualControlRepository extends JpaRepository<ManualControl, Integer> {
    ManualControl getManualControlByBox(Box box);
}
