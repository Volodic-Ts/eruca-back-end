package com.example.eruca.repository;

import com.example.eruca.entity.Box;
import com.example.eruca.entity.ModulesState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModulesStateRepository extends JpaRepository<ModulesState, Integer> {
    ModulesState getModulesStateByBox(Box box);
}
