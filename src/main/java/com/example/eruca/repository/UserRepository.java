package com.example.eruca.repository;

import com.example.eruca.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User getUserByLogin(String login);
    User getUserByEmail(String email);
    User getUserByCode(int code);
    User getUserByUserId(long id);
    Optional<User> findUserByLogin(String login);
}
