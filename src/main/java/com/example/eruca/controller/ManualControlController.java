package com.example.eruca.controller;

import com.example.eruca.entity.ManualControl;
import com.example.eruca.service.ManualControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/manual")
public class ManualControlController {

    @Autowired
    private ManualControlService manualControlService;

    @PostMapping("/add/{id}")
    public void createManualControl(@PathVariable(name = "id") long id, @RequestBody ManualControl manualControl) {
        manualControlService.createManualControl(id, manualControl);
    }

    @GetMapping("/get/{id}")
    public ManualControl getBoxManualControl(@PathVariable(name = "id") long id) {
        return manualControlService.getBoxManualControl(id);
    }

    @PutMapping("/edit/{id}")
    public void updateManualControl(@PathVariable(name = "id") long id, @RequestBody ManualControl manualControl) {
        manualControlService.addValuesToManualControl(id, manualControl);
    }

    @PutMapping("/remove/{id}")
    public void removeManualControl(@PathVariable(name = "id") long id) {
        manualControlService.removeManualControl(id);
    }
}
