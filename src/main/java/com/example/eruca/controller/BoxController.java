package com.example.eruca.controller;

import com.example.eruca.entity.Box;
import com.example.eruca.service.BoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/boxes")
public class BoxController {

    @Autowired
    private BoxService boxService;

    @GetMapping("/get")
    public Map<String, List<Box>> getAllUserBoxes() {
        Map<String, List<Box>> response = new HashMap<String, List<Box>>();
        response.put("boxes", boxService.getAllUserBoxes());
        return response;
    }

    @GetMapping("/get/all")
    public List<Box> getAllBoxes() {
        return boxService.findAllBoxes();
    }

    @GetMapping("/get/one/{id}")
    public Box getOneBox(@PathVariable(name = "id") long id) {
        return boxService.findBoxById(id);
    }

    @PostMapping("/add")
    public void addNewBox(@RequestBody Box box) {
        boxService.addNewBox(box);
    }

    @PutMapping("/edit/{id}")
    public void editBox(@PathVariable(name = "id") long id, @RequestBody Box box) {
        boxService.editBox(id, box);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteBox(@PathVariable(name = "id") long id) {
        boxService.deleteBox(id);
    }
}
