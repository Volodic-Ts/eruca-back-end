package com.example.eruca.controller;

import com.example.eruca.entity.TechCard;
import com.example.eruca.service.TechCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/card")
public class TechCardController {

    @Autowired
    private TechCardService techCardService;

    @GetMapping("/get/all")
    public List<TechCard> getAllTechCards() {
        return techCardService.getAllTechCards();
    }

    @GetMapping("/get/{id}")
    public Map<String, TechCard> getBoxTechCard(@PathVariable(name = "id") long id) {
        Map<String, TechCard> response = new HashMap<>();
        response.put("techCard", techCardService.getBoxTechCard(id));
        return response;
    }

    @PostMapping("/add/{id}")
    public void addNewTechCard(@PathVariable(name = "id") long id, @RequestBody TechCard techCard) {
        techCardService.addTechCard(id, techCard);
    }

    @PutMapping("/edit/{id}")
    public void editTechCard(@PathVariable(name = "id") long id, @RequestBody TechCard techCard) {
        techCardService.editTechCard(id, techCard);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTechCard(@PathVariable(name = "id") long id) {
        techCardService.deleteTechCard(id);
    }
}
