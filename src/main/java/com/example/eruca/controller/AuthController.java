package com.example.eruca.controller;

import com.example.eruca.configuration.jwt.JwtUtil;
import com.example.eruca.entity.User;
import com.example.eruca.helpers.AuthRequest;
import com.example.eruca.helpers.AuthResponse;
import com.example.eruca.helpers.MyUserDetailsService;
import com.example.eruca.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    private String token = null;

    @PostMapping("/auth")
    public ResponseEntity<?> createToken(@RequestBody AuthRequest request) throws BadCredentialsException {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
            );
        } catch (BadCredentialsException e) {
            e.printStackTrace();
        }

        final UserDetails userDetails = myUserDetailsService.loadUserByUsername(request.getUsername());
        final String token = jwtUtil.generateToken(userDetails);

        setAuthToken(token);

        return ResponseEntity.ok(new AuthResponse(token));
    }

    public String getAuthToken() {
        return this.token;
    }

    private void setAuthToken(String token) {
        this.token = token;
    }
}
