package com.example.eruca.controller;

import com.example.eruca.entity.ModulesState;
import com.example.eruca.service.ModulesStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/modules")
public class ModulesStateController {

    @Autowired
    private ModulesStateService modulesStateService;

    @GetMapping("/get/all")
    public List<ModulesState> getAllModulesState() {
        return modulesStateService.getAllModulesState();
    }

    @GetMapping("/get/{id}")
    public ModulesState getBoxModulesState(@PathVariable(name = "id") long id) {
        return modulesStateService.getBoxModulesState(id);
    }

    @PostMapping("/add/{id}")
    public void addModulesState(@PathVariable(name = "id") long id, @RequestBody ModulesState modulesState) {
        modulesStateService.addModulesState(id, modulesState);
    }

    @PutMapping("/edit/{id}")
    public void editModulesState(@PathVariable(name = "id") long id, @RequestBody ModulesState modulesState) {
        modulesStateService.editModulesState(id, modulesState);
    }
}
