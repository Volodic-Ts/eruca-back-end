package com.example.eruca.controller;

import com.example.eruca.entity.TechCardTemplate;
import com.example.eruca.service.TechCardTemplatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/templates")
public class TechCardTemplatesController {

    @Autowired
    private TechCardTemplatesService service;

    @GetMapping("/get/all")
    public Map<String, List<TechCardTemplate>> getAllTemplates() {
        Map<String, List<TechCardTemplate>> response = new HashMap<>();
        response.put("templates", service.getAllTemplates());
        return response;
    }

    @GetMapping("/get/one/{id}")
    public Map<String, TechCardTemplate> getOneTemplate(@PathVariable(name = "id") int id) {
        Map<String, TechCardTemplate> response = new HashMap<>();
        response.put("template", service.getOneTemplate(id));
        return response;
    }

    @PostMapping("/save")
    public void saveAllTemplates(@RequestBody List<TechCardTemplate> templates) {
        service.addTechCardTemplates(templates);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteSavedTemplate(@PathVariable(name = "id") int id) {
        service.deleteSavedTemplate(id);
    }

    @PutMapping("/edit/id/{n}")
    public void updateTemplatesIds(@PathVariable(name = "n") int n) {
        service.editSavedTemplatesIds(n);
    }

    @DeleteMapping("/delete/all")
    public void deleteAllTemplates() {
        service.deleteAllTemplates();
    }
}
