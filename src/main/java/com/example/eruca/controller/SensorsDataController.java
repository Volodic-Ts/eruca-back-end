package com.example.eruca.controller;

import com.example.eruca.entity.SensorsData;
import com.example.eruca.service.SensorsDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sensors")
public class SensorsDataController {

    @Autowired
    private SensorsDataService sensorsDataService;

    @GetMapping("/get/all")
    public List<SensorsData> getAllSensorsData() {
        return sensorsDataService.getAllSensorsData();
    }

    @GetMapping("/get/{id}")
    public Map<String, SensorsData> getSensorsData(@PathVariable(name = "id") long id) {
        Map<String, SensorsData> response = new HashMap<String, SensorsData>();
        response.put("sensorsData", sensorsDataService.getBoxSensorsData(id));
        return response;
    }

    @PostMapping("/add/{id}")
    public void addSensorsData(@PathVariable(name = "id") long id, @RequestBody SensorsData sensorsData) {
        sensorsDataService.addSensorsData(id, sensorsData);
    }

    @PutMapping("/edit/{id}")
    public void editSensorsData(@PathVariable(name = "id") long id, @RequestBody SensorsData sensorsData) {
        sensorsDataService.editSensorsData(id, sensorsData);
    }
}
