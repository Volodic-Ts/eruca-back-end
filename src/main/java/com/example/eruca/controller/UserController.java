package com.example.eruca.controller;

import com.example.eruca.entity.User;
import com.example.eruca.entity.UserDTO;
import com.example.eruca.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/reg")
    public void addNewUser(@RequestBody UserDTO user) {
        userService.register(user);
    }

    @PostMapping("/confirm/{code}")
    public void confirmUser(@PathVariable(name = "code") int code) {
        userService.confirmAccountByCode(code);
    }

    @GetMapping("/user/get/{login}")
    public User getUser(@PathVariable(name = "login") String login) {
        return userService.getUser(login);
    }

    @GetMapping("/users/get")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @DeleteMapping("/users/delete/{login}")
    public void deleteUser(@PathVariable(name = "login") String login) {
        userService.deleteUser(login);
    }

    @DeleteMapping("/users/delete-id/{id}")
    public void deleteUser(@PathVariable(name = "id") long id) {
        userService.deleteUser(id);
    }
}
