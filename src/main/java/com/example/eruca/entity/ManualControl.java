package com.example.eruca.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Data
@Table(name = "manual_control")
public class ManualControl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "manual_control_id")
    private int manual_control_id;

    @Column(name = "brightness_min")
    @Nullable
    private int brightnessMin;

    @Column(name = "brightness_max")
    @Nullable
    private int brightnessMax;

    @Column(name = "fluid_level_min")
    private int fluidLevelMin;

    @Column(name = "fluid_level_max")
    private int fluidLevelMax;

    @Column(name = "temperature_min")
    private double temperatureMin;

    @Column(name = "temperature_max")
    private double temperatureMax;

    @Column(name = "humidity_min")
    private double humidityMin;

    @Column(name = "humidity_max")
    private double humidityMax;

    @Column(name = "soil_moisture_min")
    private int soilMoistureMin;

    @Column(name = "soil_moisture_max")
    private int soilMoistureMax;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "box_id", nullable = false)
    @JsonIgnore
    private Box box;

    public int getBrightnessMin() {
        return brightnessMin;
    }

    public void setBrightnessMin(int brightnessMin) {
        this.brightnessMin = brightnessMin;
    }

    public int getBrightnessMax() {
        return brightnessMax;
    }

    public void setBrightnessMax(int brightnessMax) {
        this.brightnessMax = brightnessMax;
    }

    public int getFluidLevelMin() {
        return fluidLevelMin;
    }

    public void setFluidLevelMin(int fluidLevelMin) {
        this.fluidLevelMin = fluidLevelMin;
    }

    public int getFluidLevelMax() {
        return fluidLevelMax;
    }

    public void setFluidLevelMax(int fluidLevelMax) {
        this.fluidLevelMax = fluidLevelMax;
    }

    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public double getHumidityMin() {
        return humidityMin;
    }

    public void setHumidityMin(double humidityMin) {
        this.humidityMin = humidityMin;
    }

    public double getHumidityMax() {
        return humidityMax;
    }

    public void setHumidityMax(double humidityMax) {
        this.humidityMax = humidityMax;
    }

    public int getSoilMoistureMin() {
        return soilMoistureMin;
    }

    public void setSoilMoistureMin(int soilMoistureMin) {
        this.soilMoistureMin = soilMoistureMin;
    }

    public int getSoilMoistureMax() {
        return soilMoistureMax;
    }

    public void setSoilMoistureMax(int soilMoistureMax) {
        this.soilMoistureMax = soilMoistureMax;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }
}
