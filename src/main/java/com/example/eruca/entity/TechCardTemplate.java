package com.example.eruca.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class TechCardTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int plantId;

    private String plantName;
    private String plantDescription;
    private String plantImageUrl;
    private int fluidLevelMin;
    private int fluidLevelMax;
    private double temperatureMin;
    private double temperatureMax;
    private double humidityMin;
    private double humidityMax;
    private double soilMoistureMin;
    private double soilMoistureMax;
}
