package com.example.eruca.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "tech_card")
public class TechCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    @JsonIgnore
    private int card_id;

    @Column(name = "brightness_min")
    private int brightnessMin;

    @Column(name = "brightness_max")
    private int brightnessMax;

    @Column(name = "fluid_level_min")
    private int fluidLevelMin;

    @Column(name = "fluid_level_max")
    private int fluidLevelMax;

    @Column(name = "temperature_min")
    private double temperatureMin;

    @Column(name = "temperature_max")
    private double temperatureMax;

    @Column(name = "humidity_min")
    private double humidityMin;

    @Column(name = "humidity_max")
    private double humidityMax;

    @Column(name = "soil_moisture_min")
    private double soilMoistureMin;

    @Column(name = "soil_moisture_max")
    private double soilMoistureMax;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "box_id", nullable = false)
    @JsonIgnore
    private Box box;

    public int getBrightnessMin() {
        return brightnessMin;
    }

    public void setBrightnessMin(int brightnessMin) {
        this.brightnessMin = brightnessMin;
    }

    public int getBrightnessMax() {
        return brightnessMax;
    }

    public void setBrightnessMax(int brightnessMax) {
        this.brightnessMax = brightnessMax;
    }

    public int getFluidLevelMin() {
        return fluidLevelMin;
    }

    public void setFluidLevelMin(int fluidLevelMin) {
        this.fluidLevelMin = fluidLevelMin;
    }

    public int getFluidLevelMax() {
        return fluidLevelMax;
    }

    public void setFluidLevelMax(int fluidLevelMax) {
        this.fluidLevelMax = fluidLevelMax;
    }

    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public double getHumidityMin() {
        return humidityMin;
    }

    public void setHumidityMin(double humidityMin) {
        this.humidityMin = humidityMin;
    }

    public double getHumidityMax() {
        return humidityMax;
    }

    public void setHumidityMax(double humidityMax) {
        this.humidityMax = humidityMax;
    }

    public double getSoilMoistureMin() {
        return soilMoistureMin;
    }

    public void setSoilMoistureMin(double soilMoistureMin) {
        this.soilMoistureMin = soilMoistureMin;
    }

    public double getSoilMoistureMax() {
        return soilMoistureMax;
    }

    public void setSoilMoistureMax(double soilMoistureMax) {
        this.soilMoistureMax = soilMoistureMax;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }
}
