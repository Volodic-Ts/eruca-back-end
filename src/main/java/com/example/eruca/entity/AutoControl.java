package com.example.eruca.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "auto_control")
public class AutoControl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auto_control_id")
    private int sensors_id;

    @Column(name = "brightness")
    private boolean brightness;

    @Column(name = "fluid_level")
    private boolean fluidLevel;

    @Column(name = "temperature")
    private boolean temperature;

    @Column(name = "humidity")
    private boolean humidity;

    @Column(name = "soil_moisture")
    private boolean soilMoisture;
}
