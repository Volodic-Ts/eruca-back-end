package com.example.eruca.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "Box")
public class Box {

    @Id
    @Column(name = "box_id")
    private long id;

    @Column(name = "box_title")
    private String title;

    @Column(name = "box_version")
    private String version;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @OneToOne(mappedBy = "box", cascade = CascadeType.ALL)
    @JsonIgnore
    private SensorsData sensorsData;

    @OneToOne(mappedBy = "box", cascade = CascadeType.ALL)
    @JsonIgnore
    private ModulesState modulesState;

    @OneToOne(mappedBy = "box", cascade = CascadeType.ALL)
    @JsonIgnore
    private TechCard techCard;

    @OneToOne(mappedBy = "box", cascade = CascadeType.ALL)
    @JsonIgnore
    private ManualControl manualControl;

    public long generateBoxId() {
        long leftLimit = 100000000000000000L;
        long rightLimit = 999999999999999999L;

        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public TechCard getTechCard() {
        return techCard;
    }

    public void setTechCard(TechCard techCard) {
        this.techCard = techCard;
    }
}
