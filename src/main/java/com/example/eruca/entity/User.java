package com.example.eruca.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long userId;

    @Column(name = "user_login")
    private String login;

    @Column(name = "email")
    private String email;

    @Column(name = "user_password")
    @JsonIgnore
    private String password;

    @Column(name = "user_roles")
    @JsonIgnore
    private String roles;

    @Column(name = "enabled")
    @JsonIgnore
    private boolean active;

    @Column(name = "validation_code")
    @JsonIgnore
    private int code;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Box> boxes;
}
