package com.example.eruca.entity;

import lombok.Data;

import java.util.regex.Pattern;

@Data
public class UserDTO {

    private String login;
    private String email;
    private String password;
    private String matchPassword;

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getMatchPassword() {
        return matchPassword;
    }

    public boolean validateEmail(String email) {
        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9\\+_-]+(\\.[A-Za-z0-9\\+_-]+)*@"
                + "[^-][A-Za-z0-9\\+-]+(\\.[A-Za-z0-9\\+-]+)*(\\.[A-Za-z]{2,})$";

        return Pattern.compile(regexPattern)
                .matcher(email)
                .matches();
    }

    public boolean validatePassword(String password) {
        if (password.length() >= 8 && password.length() <= 16) {
            for (int i = 0; i < password.length(); i++) {
                if (!" ".equals(password.charAt(i))) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean comparePasswords(String password, String matchPassword) {
        return password.equals(matchPassword);
    }
}
