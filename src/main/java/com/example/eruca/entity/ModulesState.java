package com.example.eruca.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "modules_state")
public class ModulesState {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "modules_id")
    @JsonIgnore
    private int modules_id;

    @Column(name = "light")
    private boolean light;

    @Column(name = "pump")
    private boolean pump;

    @Column(name = "cooler")
    private boolean cooler;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "box_id", nullable = false)
    @JsonIgnore
    private Box box;

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public boolean isLight() {
        return light;
    }

    public void setLight(boolean light) {
        this.light = light;
    }

    public boolean isPump() {
        return pump;
    }

    public void setPump(boolean pump) {
        this.pump = pump;
    }

    public boolean isCooler() {
        return cooler;
    }

    public void setCooler(boolean cooler) {
        this.cooler = cooler;
    }
}
