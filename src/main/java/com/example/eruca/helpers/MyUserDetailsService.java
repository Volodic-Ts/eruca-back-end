package com.example.eruca.helpers;

import com.example.eruca.entity.User;
import com.example.eruca.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = repository.findUserByLogin(username);

        user.orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));

        return user.map(MyUserDetails::new).get();
    }
}
