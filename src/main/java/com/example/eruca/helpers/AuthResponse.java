package com.example.eruca.helpers;

import lombok.Data;

@Data
public class AuthResponse {
    private final String jwt;
}
